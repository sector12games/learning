DocumentDB Issues
 - Optimistic Concurrency is handled via something called an ETag (http entity tags)
   - Every client keeps track of collection/document "read ids". If the read id is out of date when you try to
     write, you have to read again first.
   - https://azure.microsoft.com/en-us/documentation/articles/documentdb-faq/

 - Getting document self-links, by querying on other elements than just "id"
   - This is a huge pain in DocumentDB
   - For example, how can I get the self-link from the following? I can't.
       Feature feature = await _client.CreateDocumentQuery<Feature>(_collection.DocumentsLink)
           .Where(f => f.Id == mappedFeatureModel.Id && f.Version == mappedFeatureModel.Version)
           .FirstOrDefaultAsync();
   - I would have to  next call the following:
       Document doc = await _client.CreateDocumentQuery(_collection.DocumentsLink)
           .Where(f => f.Id == feature.Id)
           .FirstOrDefaultAsync();
    - But that's no good because it wrecks concurrency (I'm searching for the verion, to make sure it's the same)
    - This could work, but GetPropertyValue is not yet supported in LinqToDocumentDB
        Document doc = await _client.CreateDocumentQuery(_collection.DocumentsLink)
            .Where(f => f.Id == feature.Id && f.GetPropertyValue<int>("Version") == 1)
            .FirstOrDefaultAsync();
    - I am forced to use the following, non-type-safe method
        Document test = await _client.CreateDocumentQuery(
            _collection.DocumentsLink,
            string.Format("SELECT * FROM f WHERE f.Version = {0}", mappedFeatureModel.Version)).FirstOrDefaultAsync();



