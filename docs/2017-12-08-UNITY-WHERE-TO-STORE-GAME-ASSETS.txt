THE PROBLEM
 - Games typically require a very large set of binary assets. There's tons of artwork
   and audio files, and they quickly fill up limited git storage. Most git hosts will
   limit your storage to less than 2GB of data, so we need a strategy for storing our
   assets.



OPTIONS
 - Multiple git repos
   - The most important part is keeping all of your binary assets out of your c# code
     repo. The assets build up really quickly and it's very difficult to remove them
     entirely from your code base while still maintaining your commit history.
   - Here's what your repo structure would look like:
     - Main code repo (only code, all assets are exported from Articy)
     - Articy repo (a curated list of all assets you'll actually use in your project)
     - Art resources repo (aggregated collection of art assets from asset store, etc)
     - Audio resources repo (again, an aggregated collection of assets)
   - The problem is that even going this route, you're still going to run into storage
     issues. For example, just one mega music pack that I downloaded from the unity
     asset store was 3.5GB when fully compressed. There's no way I can keep this as
     a single asset. This means I end up splitting my collection apart into more and
     more asset repos. It's not a perfect solution.
 - Google drive
   - Store all potential unity assets (art packs, audio packs, etc) in a single
     aggregated zip file on google drive. Drive is super cheap ($2 a month for 100GB).
   - You would still have a git repo for your code and another git repo for your articy
     draft content. The articy repo could also contain versioned mockups and any custom
     assets that you make yourself.
   - I may still run into size issues when it comes to the articy repo, as any assets
     I'm actually using in my game will be committed to the repo. I haven't quite
     decided on a solution if this actually does come to be an issue.



FINAL REPO/ASSET BREAKDOWN
 - Main code repo
 - Articy repo
 - Google drive zip file that contains all potential resources for my project

 - Remember that the most important thing is to not commit any binary assets to the
   code repo.


