HOSTING FRONT-END APPS

 - Firebase
   - Provides endpoints to a database. That's it for a backend.
   - Provides static-content-only CDN hosting. 
     - When you run "firebase deploy", your javascript, images, css, html, etc will get pushed out to a CDN (fastly)
   - Users will hit the CDN in their region, get the javascript, and then the javascript will talk to the firebase database
   - This is fantastic for a truely front-end-only app, that just needs a database, and all calculations and 
     processing can be done on the client side, in javascript.
   - If we need to do any server-side processing at all, we need to set up a true web server (on another host)
     - We can still host our ember/angular app on firebase, and have data access there as well
     - Our ember/angular app will need to be made aware of our other host however (php/node.js/etc)

   - The major consideration here, of course, would be authentication, which usually cannot be done (and shouldn't
     be done - we don't want passwords stored in plain text) entirely on the client side. 
   - To circumvent this, Firebase provides authentication servers. Here's how it works
     - Create your firebase app
     - You create a twitter app
       - In your twitter app, you configure a "callback url" - point it to auth.firebase.com/your-app/...
     - Log into your firebase app, and in the "Login & Auth" section, you can enable twitter authentication,
       and enter your api key and secret.
     - You use the firebase javascript client to authenticate, which talks to firebase auth server, which talks to
       Twitter auth servers. If successful, you'll get back an "accessToken" and an "accessTokenSecret"
       - You can then use these tokens to retrieve tweets, etc, but I'm not sure how using plain ajax. This is
         much, much easier using a javascrip twitter client, which would again, require a separate server-side host.
       - http://stackoverflow.com/questions/16072282/can-i-use-firebase-simple-login-to-access-twitter-api
    - After writing all of this, it's clear that Firebase didn't create twitter auth for the purpose of searching
      twitter - they created it for the purpose of allowing users to log into firebase using twitter's authentication.
      - Access to the authentication tokens wasn't granted until later (you can see that below)
        http://stackoverflow.com/questions/16072282/can-i-use-firebase-simple-login-to-access-twitter-api

CONSIDERATIONS FOR THE TWITTER APP

 - I can attempt to use firebase to do my twitter auth, but it will be hard without a wrapper client
   - This isn't a silver bullter - it will only work with twitter and maybe a few others (github). If I ever want
     to do this sort of thing against another, less-known api, I won't be able to use firebase.
 - I think it's best to just bite the bullet and host a small app on heroku.
   - I have to have a server side app, small as it might be, so I might as well just host ember along with it
   - Ember could techincally be hosted on firebase, and it might actually be a good idea - if I ever need to use
     a database for any reason.

HOW TO HOST AN EMBER APP

 - All you essentially need to do is upload your /dist folder to a server somewhere. That's it.
 - My twitter app really shouldn't need nginx on the front and node on the back. One or the other should work,
   and I'll need node for my twitter calls. So maybe look into simply creating a node server (with express) that
   can serve static content (my ember /dist).
   - OR look into hosting my ember site on firebase (dead simple, CDN benefits, etc), and just host my simple node.js
     app on heroku. This is probably the best idea, and allows me to easily add backend-data if needed (saving stats, etc)



