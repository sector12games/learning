TEAMCITY
 - All domains should now be building to the RelayHealth nuget feed, and not CommonWell
   - Gyan will try to cleanup the CommonWell feed next week
   - Theoretically, it would be ok to remove CommonWell from our Visual Studio nuget feeds (theoretically)
 - For SwitchReporter, teamcity was set to target the .nuspec file directly, rather than the .csproj file.
   This meant RelayHealth.Platform.SwitchReporter.dll was not included in the .nupkg file. 
   - Someone changed this about 8 months ago (November 20th, 2014). That's when nuget builds stopped including
     the dll.
 - In addition, SwitchReporter was not configured correctly in TeamCity. There was no release build for it,
   so developers were manually removing the -develop tag from the integration build. This caused issues because
   the new release build version numbers were lower than the old, renamed -develop integration version numbers.

 - Team Builds (when committed to ascent repo):
   - 0.0.x.x
 - Integration Builds (when pulled into CommonWell/Develop):
   - 1.0.x.x-develop
 - Release Builds (when pulled into CommonWell/Release)
   - 1.0.x.x