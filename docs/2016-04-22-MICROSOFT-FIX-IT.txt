HOW TO UNINSTALL A PROGRAM WHO'S UNINSTALLER IS BROKEN
 - I had this happen to me with MetaScan
 - I could not uninstall via the installer provided (I got an error)
 - I could not uninstall using programs/feature (I got the same error)

 - First, I manually deleted all files in ProgramFiles(x86)
   - This took some work and a few restarts because the services were running, so I couldn't delete everything, but I couldn't
     stop the services because they would hang and I'd receive an error. I deleted what I could, restarted, deleted more, etc.
 - Next, I downloaded "Microsoft Fix It" to clean up the registry and get the item removed from add/remove programs
   - https://support.microsoft.com/en-us/mats/program_install_and_uninstall
 - The app scans for problem files, but in my case, it was not found. I had to enter the "product id"
   - I found the product id by opening the registry and going to:
     HKEY_LOCAL_MACHINE\SOFTWARE\Wow6432Node\Microsoft\Windows\CurrentVersion\Uninstall
   - Once there, I searched for "MetaScan"
   - I copied the guid of the key, which was the "product id"
   - It looked something like {047904BA-C065-40D5-969A-C7D91CA93D62}
   - Now, I pasted that into Microsoft Fix It. It ran successfully.
 - Now, the item was still in add/remove programs. When trying to uninstall, it gave me an error message, something along the
   lines of "Wait until the program is done being modified by another process"
 - Restart the computer
 - Now, I can finally go into add/remove programs and run the uninstaller. It was gone for good.