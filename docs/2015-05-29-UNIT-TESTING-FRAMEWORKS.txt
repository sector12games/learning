- Isolation Frameworks
  - Are ultimately pretty evil. Avoid using them if possible.
  - They promote bad coding practices.
  - If your code is written correctly, you should never need an isolation framework.
    A mocking framework should work just fine.
  - Isolation frameworks should only be used to add new unit testing to a legacy application
    that was not built with unit testing in mind.
  - Isolation frameworks do a lot of "reflection magic".
  - TypeMock (commercial license requried)
  - MS Fakes (previously MS Moles)

Mocking Frameworks
  - Moq
  - FakeItEasy
  - NSubstitute
  - RhinoMocks (old, don't use)