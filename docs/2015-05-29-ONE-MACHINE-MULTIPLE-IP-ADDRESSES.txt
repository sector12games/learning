Localhost and multiple IP addresses
  - Localhost always points to the loopback IP, which is 127.0.0.1
    - This is a software-only IP address, implemented entirely in the OS
    - It passes packets immediately back up the software stack, like it came from another device,
      but no packets were actually sent to any network interface controller.
    - Localhost/loopback always means "this machine"

  - Keep in mind that localhost is not tied to the IP address of the machine
    - For instance, machines can have multiple IP address. Which one does "localhost" point to?
      - NONE. It points to this machine. Once the machine is determines, all routing is
        based on ports.
  - Essentially, it helps to keep machines (ips) and ports competely separate.
  - The IP is used to determine the correct machine. Once that's determined, ports are used
    to determine the proper service.
  
  - EXAMPLE:
    - TEST01 has ~20 different IP addresses. One for each service.
    - Each service can have a localhost binding. However, each service must be running on
      its own unique port. This means that each IIS site's localhost binding will need to
      point to the correct port.
    - For each service, localhost will point to "this machine". The service's IP address
      isn't even really needed, because we already know the machine we want.
    - Once the machine is determined, only the port matters.

Multiple IIS sites, one port
  - This is possible. For instance, if I'm running several internet sites, all could go through port 80.
    - For this to work properly, you need to configure host headers (separate hostnames for each site).
    - Apache/IIS/etc will handle the setup of these.

IP Address 0.0.0.0
  - This is a non-routable, invalid or unknown IP address
  - It is typically used to represent "all IPs on the local machine"
  - IIS express uses this by default, but we should never see this in production

Why use multiple IP addresses for a single server
  - Main reasons:
    - In order to simulate internal/external domains.
      - In production, we have 6 external and 6 internal servers (I think this is correct)
      - These internal and external servers will have different IPs, so we need to simulate
        this in our test environment, even though it's only a single machine.
    - In order to host multiple SSL sites on the same machine
      - The internal/external sites will need to have different certs
    - If we're already splitting internal and external, we might as well split out
      each service into its own IP to keep things completely separate and easily managable.
  - Lots more reasons, not necessarily related to RelayHealth/Commonwell:
    http://unix.stackexchange.com/a/127735

  - These days, TSL is becoming the standard over SSL, and multiple TSL sites can
    easily be hosted on the same machine. 
    - This means the "to host multiple SSL" reason is only really relevant for older systems.

Ports vs Sockets
  - Machines can have 0-65535 ports (16bit number)
  - Only one service can be running (listening) on each port
  - Sockets are an "instance" of a port
  - Both ends of the socket can send/receive data to each other
  - Sockets contain 5 things:
    - Protocol (tcp/udp/etc)
    - Source IP Address
    - Source Port
    - Destination IP Address
    - Destination Port
  - The file/data that contains this information is called a file descriptor
  - There's no hard limit to how many simultaneous sockets can be in play on a single port, however:
    - File descriptors will be unique. This means a single client application won't open more than one
      connection to the same server and port, from the same client and port. The client can create another
      connection from another source port if it likes.
    - The machine has a configurable max file descriptor limit
      - In linux, I've read that this number usually defaults to 1024. It looks like upper limits
        differ depending on linux distro, but are typically somewhere between 32k-70k


