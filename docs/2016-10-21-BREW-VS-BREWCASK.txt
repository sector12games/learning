HOMEBREW (BREW)
 - http://brew.sh/
 - Likely the best package manager for macOS at this point in time (October 2016). There are multiple
   other competitors, including Nix, PkgSrc, Flatpak, but none seem to be as good as homebrew.
 - Homebrew typically only deals with command line programs. It is unable to install tools that require
   a GUI installation, such as Google Chrome.
 - Any packages installed with HomeBrew can easily be updated, rolled back, or deleted.
 - Packages are called "formula/formulae"



HOMEBREW CASK (BREW CASK)
 - https://caskroom.github.io/
 - This is a completely separate project, that extends Homebrew, desigend for installing graphical programs
   that typically require a GUI installation.
 - The cask team does not coordinate in any way with the core brew team
 - Homebrew cask will actually download and run the GUI installer
 - Packages installed with homebrew cask do not have any way of updating/upgrading. It is assumed that
   most of the packages (like Chrome) will contain automatic upgrade systems.
 - Packages are called "cask/casks"
 - More info:
   http://stackoverflow.com/questions/31968664/upgrade-all-the-casks-installed-via-homebrew-cask



RECOMMENDATIONS
 - Occasionally, packages exist in both Brew and BrewCask. When in doubt, always prefer Brew.
 - Homebrew is typically more up to date (so I've heard)



ADDITIONAL INFO
 - http://apple.stackexchange.com/questions/125468/what-is-the-difference-between-brew-and-brew-cask
 - https://www.quora.com/What-is-the-difference-between-brew-install-git-and-brew-cask-install-git


