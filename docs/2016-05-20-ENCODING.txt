HTML CHARSET AND FONTS
 - In HTml5, we always want to use <meta charset="utf-8" /> 
 - Utf8 is the best and covers virtually all characters in all languages without complexities of utf16,32
 - When the browser first loads a page, it takes a look at the charset to know what symbols it will be dealing with
   - From there, it finds the font file specified (or default font) and tries to get the symbols to draw on the page
     using the combination of the font file and utf mappings.
   - If the symbol you're trying to draw cannot be found in the font file, some browsers will try to pull the symbol
     from other default font files. This does not happen all the time, so you'll sometimes see square boxes or question marks.
   - Fore more info, see the following:
     https://www.w3.org/International/questions/qa-what-is-encoding



HTML SPECIAL CHARACTER CODES
 - All ASCII characters have a special html number, and optionally a special html name (for convenience)
 - The more common asci characters (a-z, 0-9) don't need to be encoded, because they're not special, and don't have any alternate meanings
 - Other characters either don't exist on a direct keyboard, or can be confused with markup (<, \, &, etc)
   - These values should be manually entered by the html developer as character codes to prevent the browser from interpreting the symbol incorectly
   - Some symbols, such as &, used to always be encoded, but now are no longer necessary in HTML5. However, you should still generally encode.
   - In general, you should go ahead and always encode, for backwards browser compatibility, consistency, and removal of doubt.
     There is a bit of debate on this, but most of what I've read is, even in HTML5, better safe than sorry.
 - If a character has both an HTML number and an HTML name, go ahead and use the name. It was just a shorthand, easier to remember name
   added to help with more common symbols: &#38; is the same as &amp;
 - For a full list of these ASCII/HTML mappings, see the following:
   http://www.ascii.cl/htmlcodes.htm



URL ENCODING
 - This is done simply to transmit data over the internet. Spcaes, for example, need to be converted into %20
 - Now days, this is almost always done automatically for you (in .NET, javascript ajax, etc). You generally don't have to worry about it.
 - However, keep in mind that there have been some changes with exactly how urls are to be encoded. For example, the W3C decided to allow
   %20 to be encoded instead as + for easier readability. However, this is only allowed in the QueryString portion (parameters), and not
   in the entire url.
   - .NET provides several different methods of url encoding, some of which will return %20, and some of which will return +
   - Although you generally won't have to worry about url encoding, when you do, be careful you're using the right format if
     communicating between systems (.net to ruby or something similar)
 - Here's a nice overview of the different types in .NET
   http://stackoverflow.com/a/11236038/4541950



HTML ENCODING
 - Used to prevent your browser from interpreting text with html in it, as actual html (html injection)
   - For exapmle, if the user is allowed to enter comments on a blog, he could technically enter <script>my malicious code</script>
   - If the website wasn't careful, the next time the page was loaded, instead of displaying the above script as text, the browser would
     actually interpret the script as real code! This means that anyone who came and viewed the comment could potentially have a
     malicious script run within their browser, without knowing it.
 - <text> becomes &lt;text&gt;
 - In the early days of MVC, we had <%= which did not perform any html encoding on anything rendered on the screen
 - Later, in MVC 2, we had <%: which automatically performed html encoding on anything rendered on the screen
 - After that, we have razor, which will automatically HTML encode all output rendered to the screen
   - MVC reads <text> from the database
   - Razor outputs the text to html like this &lt;text&gt;
   - The browser sees that, and displays this on the screen <text>
   - If we want to actually display &lt;text&gt; on the screen, we can call @Html.Encode(myValue)
   - If we call @Html.Encode("<text>"), it will encode twice, and return &amp;lt;text&amp;gt; (because it convers & into &amp the second time)
     - The Html.Encode encodes once
     - The razor engine @ encodes a second time
   - Html.Raw() returns an IHtmlString, not a String, so the razor engine @ knows not to encode the results
     - Html.Raw() should be used if you actually WANT the html code entered by a user to be interpreted as real html by the browser.
     - Use with caution, as this opens you up to the vulnerabilities described above.
     - Another, very good reason to use this, is with @Url.Action. Calling @Html.Raw(@Url.Action()) fixes a "bug" (maybe as intended)
       where the & symbol in query strings actually gets turned into &amp; This is faulty logic in mvc Url.Action in my opinion, but
       it is what it is. Keep in mind that this does not open us up to any vulnerabilities because we're giving it a string, the user
       does not have any input here. No possibility for html injection.
       http://stackoverflow.com/questions/7336532/how-do-i-pass-correct-url-action-to-a-jquery-method-without-extra-ampersand-trou
 - For more info on Html Encoding and MVC:
   http://weblogs.asp.net/scottgu/new-lt-gt-syntax-for-html-encoding-output-in-asp-net-4-and-asp-net-mvc-2
   http://blog.slaks.net/2011/01/dont-call-htmlencode-in-razor.html

