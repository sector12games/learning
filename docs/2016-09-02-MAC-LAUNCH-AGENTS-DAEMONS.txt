BASIC INFO STARTUP APPS ON A MAC
 - Startup apps
   - Apple -> System Preferences -> Users & Groups -> Select a User -> Login Items
   - "Hide" means that the program will still start, but will not pop up on screen at startup (it will be minimized)
   - If you want to add or remove items, click the +/- buttons at the bottom
 - LAUNCH AGENTS
   - Start on user login
   - Continue running when your screen is locked, but shut down on logout
   - Seemingly cannot be shared by multiple users (this was the problem with Rosetta Stone)
   - There is a directory for "shared" (multi-user) launch agents, and a directory for user-specific agents
   - Launch agent processes are luanched by placing a ".plist" file into a launch agent/daemon directory
 - LAUNCH DAEMONS
   - Start on computer startup
   - Can be used/shared by all users, regardless of admin priviledges (seems that way at least)
   - Will only have one instance running on the machine regardless of how many users are currently logged in



LAUNCH AGENT/DAEMON LOCATIONS
 - /Library/LaunchDaemons		(ONE INSTANCE PER MACHINE, SINGLE INSTANCE SHARED BY ALL USERS)
 - /Library/LaunchAgents		(SHARED/MULTI-USER LAUNCH AGENTS)
 - /Users/shaun/Library/LaunchAgents	(USER SPECIFIC LAUNCH AGENTS)



HOW TO VIEW RUNNING PROCESSES
 - Run "Activity Monitor"
 - You can see all processes (including launch agent (background) processes running for your user)
 - I have not found a way to view processes running as Daemons, but I haven't tried looking very hard.



ROSETTA STONE ERROR DESCRIPTION
 - Rosetta Stone launches a background process "RosettaStoneDaemon"
 - This process is installed as a launch agent (shared by all users)
 - The process runs regardless of whether or not I have Rosetta Stone running
 - If I were logged in as user "shaun", and I close my lid, which locks my screen but does not log me out,
   the process keeps running.
 - The process is running under my name, so any other users on the machine do not have access to it
 - If user "makoy" then logs in, the "RosettaStoneDaemon" will not start up because it's detected as already running
 - She can start RosettaStone ok, but the microphone is not detected or usable. This is because the daemon is running
   under user "shaun" but she's trying to access as user "makoy".

 - An easy way around this is to make sure user "shaun" is logged out every time. However, this is unrealistic.

 - The real fix was to move the launch agent into the LaunchDaemons directory, to have it run with a single instance,
   that is usable by all users.
 - Both user "shaun" and user "makoy" can now use ROsettaStone and the microphone without issues, despite both being
   logged in at the same time.



