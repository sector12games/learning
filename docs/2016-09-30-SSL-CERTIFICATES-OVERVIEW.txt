SSL, TLS, HTTPS
 - TLS is the new name for SSL. Namely, SSL protocol got to version 3.0; TLS 1.0 is "SSL 3.1".
 - HTTPS is HTTP-within-SSL/TLS. SSL (TLS) establishes a secured, bidirectional tunnel for 
   arbitrary binary data between two hosts
 - The purpose of https/ssl is to provide a secure, encrypted connection between client and server
 - More info:
   http://security.stackexchange.com/questions/5126/whats-the-difference-between-ssl-tls-and-https



PULIC AND PRIVATE KEYS
 - The two keys are mathmatically linked
 - Each key can decrypt data encrypted by the other
 - Typically, data is encrypted with the private key and decrypted with the public, but it goes both ways 
   (browser clients encrypt using the public key to initiate a secure ssl handshake)



WHAT'S IN A DIGITAL SIGNATURE
 - A hash of the data the signature is attached to (encrypted using the private key)
 - The public key that can be used to decrypt the hash
 - The hashing algorithm used to hash the document (used to re-hash the document and verify the hashes match)



PURPOSE OF DIGITAL SIGNATURES
 - Prove the data really came from the person you received it from (public key can decrypt properly so hashes match)
 - Ensure the message hasn't changed in transit (data hasn't changed, so hashes match)
 - Prove you are who you say you are (Cert Authority - any hacker can say he's "google.com")

 - Work on the Prover's end:
   - The "prover" (person that want to prove he's legit) has a public and a private key
   - The prover has a huge document, so he hashes it first to make it more manageable. The has is one-way only.
   - The prover uses his private key to encrypt the hash. The encrypted hash can only be decrypted using the public key.
   - The prover also includes the hashing algorithm as part of the sigature

 - Work on the receiver's end:
   - The receiver receives the document, along with the digital signature
   - The digital signature contains the public key, the original hash of the data, and the hashing algorithm used
   - The receiver decrypts the hash using the public key
   - The receiver uses the hash algorithm to re-hash the data/document
   - The receiver checks to make sure the new hash is the same as the original hash. 
   - If the hashes match, we know that:
     - The document hasn't changed in transit (otherwise the hashes would be different)
     - The document really did come from the "prover" because the public key was able to properly decrypt the hash.
       If the wrong public key was used (someone else's - possibly a hacker's) the decrypted hash wouldn't match the
       hash of the document (only the original public key can decrypt the original private key's message).
   - If the hashes don't match, we know that either problem could have occurred (we don't care which):
     - The document changed in transit (modified/swapped out by a hacker)
     - The public key was swapped out in transit in some sort of hacking attempt

 - How do we know the "prover" is legit and isnt impersonating someone else?
   - A hacker pretending to be "google.com" can send us a message using the public/private key method
   - We can verify that the message hasn't changed and that the message really did come from the hacker (but we 
     thought it came from google.com!)
   - We need someone we trust to tell us that yes, this public key really is from google and not from a hacker
   - This other person needs to be a trustworthy company that is regularly audited, and they need to do an
     extensive background check on google.com (call google in person, get physical documents, etc)
   - If this other person (who I trust) trusts the public key sent to me by "google.com", then I can be reasonably
     sure google really is google.
   - This other person (that I trust) is a certificate authority



CERTIFICATE AUTHORITIES
 - Continuing the example from above, the cert authority gives "google.com" a certificate with a public 
   key (google generates the private key, and a certificate request - only the request is sent to the cert 
   authority, which is translated into a cert with a public key only).
 - This cert is signed by the CA (just the same as google signed the message/document it sent to you).
 - Root CA certs are not signed by anyone - they are the top. You have to decide if you want to trust it or not.
 - Browsers will include a good list of Root CAs they trust
 - Thus, here is the chain of trust
   I trust Chrome -> Chrome trusts a Root CA -> the root CA trusts a TON of other company's certs



SSL HANDSHAKE OVERVIEW
 - Initiated by the client
   - Client sends the server some details
     - Which version of SSL/TLS it is running
     - What ciphersuites it wants to use
     - What compression methods it wants to use
   - Server selects the highest version of SSL/TLS both client and server support, selects the ciphersuite (hashing
     algorithm) and optionally the compression method
   - Server sends it's public cert back to the client
   - Client browser makes sure it's a trusted cert, and there's no domain mismatch, or gives you an error/warning if not
   - After verifying the cert is ok, the client creates a symmetric encryption key which will be used for future
     communication (because it's much, much faster than private/public assymmetric encryption).
   - The client encrypts this symmetric key using the public key, and sends it to the server
   - The server decrypts it using the private key
   - The server uses the symmetric key to send an encrypted confirmation message to the client
   - The client receives it, verifies it worked, and now the browser and server can communicate securely
     using symmetric encryption.
 - More info
   http://security.stackexchange.com/questions/20803/how-does-ssl-tls-work



CERT SERIAL NUMBERS VS THUMBPRINTS
 - Thumbprints are unique world wide
   - This is a hash of the entire cert (all fields, including the signature)
   - Typically SHA1/SHA2
 - Serial numbers are a number meant to be unique within the CA
   - These are not unique world wide
   - They're simply an additional id number meant for use by the CA
   - When self signing certs, people usually start at (or accidentally reuse) 0 or 1
   - I believe browsers will give errors if two certs with the same serial number and CA are found,
     so be careful to make these numbers unique if you're starting your own internal CA.
 - More info:
   http://security.stackexchange.com/questions/35691/what-is-the-difference-between-serial-number-and-thumbprint



CHKSUM/MD5SUM/SHA1SUM
 - These are linux command line tools to run hashes and hash checks on data you've downloaded
 - This is particularly useful for very large files
 - I'm not sure which method is preferred these days. All 3 programs will do the same thing, but with
   different hashing algorithms.

 - A hash is provided with a file download for checking data integrity
 - When you've downloaded the complete file, hash it, to see if it matches the original hash provided
 - If not, your download was either modified in transit or some packets are missing and you should re-download
   before installation.


 