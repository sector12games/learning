DocumentDB
 - McKesson firewall issues are causing DirectTCP issues, it works from home
 - Biggest bottleneck is the collection pricing tier - S1 is very slow
   - Solution will be to set all collections to S3 at upload time, then revert to S1 after completion
 - Partitioning data across multiple collections also significantly increases upload time. However,
   merging partitions is currently not a simple process, and must be done with the .NET SDK.
   - Overall, it's not worth the hassle
 - Preventing automatic key generation also slightly speeds up the migration, and prevents duplicates
   if you run the script again. New records will be inserted, but old ones will throw an error. Because
   no writes actually occur for all of the duplicates, the speed to run the script a second time around
   is about half.
   - You must provide a unique key (_id in our case) if you disable automatic key generation