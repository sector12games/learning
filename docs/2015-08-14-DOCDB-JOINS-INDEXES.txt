DocumentDB Joins
 - Joins are only possible within a single collection. You cannot join across collections.
   - If you want to do something like this, you can execute two queries, and join them manually,
     either in your c# code, or in a stored procedure (javascript).
 - The purpose of joins in DocumentDB is to join different level depths into a flat result.
 - Family (array of families)
   - Children (array of children, for each family)
     - Pets (array of pets, for each child)
 - In the above example, we use the syntax "JOIN ... IN ... " to join family, children, and pets together
 - Select f.name, c.name, p.name
   FROM families f
   JOIN c in f.children
   JOIN p in c.pets
   WHERE p.name = "Shadow"

Indexing Policies
 - On the fly indexing was just recently added to documentdb (~July/August 2015)
   - You can now update your index policy for a collection via the azure preview portal
   - However, error handling is terrible. If there are any errors, it will silently fail.
   - The new index policy is not added immediately, instead, it is transformed over time, and added after complete
     - This allows you to change your indexing policy while docdb is still live an accepting reads and writes
     - You can view the status of the update by looking at the "Index Transformation Progress" column where you
       select your collection.
 - All documents are automatically hash-indexed by using a wild-card indexing policy
   - You can change the precision of the index if desired
   - Hash precisions, in real-world cases, rarely require more than a precision of 3
   - If you need a range index, you can add it yourself, and this is where you might want to adjust your precision
   - Numbers can take a precision of 1-8, or -1 for max
   - String can take a precision of 1-100, or -1 for max
   - Ordreby queries on strings apparently require -1
 - What indexes are good for:
   - Hash indexes
     - Equality checks
   - Range indexes
     - Equality checks
     - Between keyword
     - Orderby keyword
     - Range queries (using >, <, >=, <=, !=)
 - In general, here are my suggestions:
   - When using range queries, use maximum precision (-1)
   - When using hash queries, use 3
 - Collections can have both hash and range queries
   - Each hash and range query can be by:
     - String
     - Number
 - I have not found a good way of testing query response time (not in the azure portal)
   - I would have to probably run web tests, with timers, to see the real value of indexes