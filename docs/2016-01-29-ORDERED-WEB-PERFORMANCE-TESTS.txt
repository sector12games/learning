CREATING TEST SETTINGS
 - In visual studio -> Right click "Solution Items" folder (top level) -> Add New Item -> Test Settings
 - From then on, just double click the .testsettings file to edit it in a gui

CREATING ORDERED TESTS
 - In visual studio -> Right click your web tests project -> Add New Item -> Search for "Ordered Test"
 - When you double click the file, you'll be able to add whichever web tests you want to the list (move 
   them to the right).

RUNNING ORDERED TESTS
 - Test Menu -> Windows -> Test Explorer
 - You have to run a build first, and then visual studio will properly find all of your ordered tests
 - Right click the individual test -> Run Selected Test
   - You don't get much of a status update, but you'll see in the "Output" window that the test is started
   - When it has finished, at the bottom of the "Test Explorer" window, you'll see a list of all the tests
     that were run, and their status.
   - You can click on each individual test to get more information as to what happened

TROUBLESHOOTING
 - "Ignoring the web test contained in file ... as it is not supported in Unit Test Explorer"
   - I think we can safely ignore this error. Everything seems to run fine despite it.
 - If you're getting errors about a webtest not being found, but the test appears to be there:
   - Most likely, the webtest's id was somehow regenerated, and that id was not copied into
     the orderedtest (for whatever reason)
   - Open up the web test in question in notepad, extract its id, and copy that into the id=
     field of the orderedtest.
   - Try running the test again, and you should be good
